# Mastodon Custom CSS

Custom CSS designed for [social.wakemu.fr](https://social.wakemu.fr)

Mastodon interface can be updated in `https://<instance.domain>/admin/settings/appearance`
